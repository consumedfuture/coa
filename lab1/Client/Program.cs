﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Int32 serverPort = 13000; 
            IPAddress localAddr = IPAddress.Parse("127.0.0.1");

            TcpClient client = new TcpClient();
            client.Connect(localAddr, serverPort); 
            NetworkStream stream = client.GetStream();

            String data;
            Byte[] bytes = new Byte[256];
            data = Console.ReadLine();
            bytes = System.Text.Encoding.UTF8.GetBytes(data); 
            stream.Write(bytes, 0, bytes.Length);

            int i = stream.Read(bytes, 0, bytes.Length); 
            data = System.Text.Encoding.UTF8.GetString(bytes, 0, i);
            Console.WriteLine(data);
            client.Close();
            Console.ReadKey();
        }
    }
}
