﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Server
{
    public class ReversePolish
    {
        public ReversePolish(string s)
        {
            ConvertToRP(s);
        }
        public bool CheckStack(string p)
        {
            if ((Equation.Count == 0) || (Priorities[Equation.Peek()] < Priorities[p]))
                return true;
            else
                return false;
        }
        public void ConvertToRP(string s)
        {
            bool may_unary = true;
            bool is_cont = true;
            for (int i = 0; i < s.Length; i++)
            {
                switch (s[i].ToString())
                {
                    case "0":
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9":
                        if (!is_cont)
                            Out += " ";
                        Out += s[i];
                        if ((i < s.Length - 1) && (Char.IsDigit(s[i + 1])))
                            is_cont = true;
                        else
                            is_cont = false;
                        may_unary = false;
                        break;
                    case "+":
                    case "-":
                    case "*":
                    case "/":
                        if ((is_cont)&&(Equation.Count>0))
                        {
                           Out += " ";
                           is_cont = false;
                        }
                        string sign;
                        if ((s[i].ToString() == "-") && (may_unary))
                            sign = "#"; 
                        else
                            sign = s[i].ToString();
                        if (CheckStack(sign))
                            Equation.Push(sign);
                        else
                        {
                            while (Equation.Count != 0)
                                if (!CheckStack(Equation.Peek()))
                                    Out += " " + Equation.Pop();
                                else break;
                            Equation.Push(sign);
                        }
                        may_unary = false;
                        break;
                    case "(":
                        Equation.Push(s[i].ToString());
                        may_unary = true;
                        break;
                    case ")":
                        while (Equation.Peek() != "(")
                            Out += " " + Equation.Pop();
                        Equation.Pop();
                        may_unary = false;
                        break;
                    default: break;
                }
            }
           while (Equation.Count > 0)
           {
              Out += " " + Equation.Pop();
           }
        }
        public float CalcFromRP()
        {
            string[] Eq = Out.Split(new char[]{' '});
            int n, arg1, arg2;
            foreach (string s in Eq)
            {
                if (int.TryParse(s, out n))
                    Numbers.Push(n);
                else{
                    arg1 = Numbers.Pop();
                    if (s != "#")
                    {
                        arg2 = Numbers.Pop();
                        switch (s)
                        {
                            case "+":
                                Numbers.Push(arg1 + arg2);
                                break;
                            case "-":
                                Numbers.Push(arg2 - arg1);
                                break;
                            case "*":
                                Numbers.Push(arg1 * arg2);
                                break;
                            case "/":
                                Numbers.Push(arg2 / arg1);
                                break;
                        }
                    }
                    else
                        Numbers.Push(-arg1);
                }
            }
            return Numbers.Pop();
        }
        Stack<string> Equation = new Stack<string>();
        Stack<int> Numbers = new Stack<int>();
        string Out;
        string[] operations = new string[5] {"+","-","*","/","("};
        Dictionary<string, int> Priorities= new Dictionary<string, int>() {{"#",4}, {"*",3}, {"/", 3}, {"+", 2}, {"-", 2}, {"(", 1}};
    }
    class Program
    {
        static void Main(string[] args)
        {
            Int32 serverPort = 13000; 
            IPAddress localAddr = IPAddress.Parse("127.0.0.1");
            TcpListener calcServer = new TcpListener(localAddr, serverPort);
            calcServer.Start(); 
            TcpClient client = calcServer.AcceptTcpClient(); 
            NetworkStream stream = client.GetStream();

            Byte[] bytes = new Byte[256]; 
            string data = null; 
            int i = stream.Read(bytes, 0, bytes.Length); 
            data = System.Text.Encoding.UTF8.GetString(bytes, 0, i);
            
            var RP = new ReversePolish(data);
            bytes = System.Text.Encoding.UTF8.GetBytes(RP.CalcFromRP().ToString());
            stream.Write(bytes, 0, bytes.Length);

            Console.ReadKey();
        }
    }
}
